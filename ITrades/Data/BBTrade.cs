﻿using System;
using System.Diagnostics;
using System.Runtime.Serialization;
namespace Trades.Data
{
    [DataContract]
    [DebuggerDisplay("{TID}|{Price}|{Date}")]
    public class BBTrade
    {
        [DataMember(Name = "date")]
        public long UnixDate { get; set; }

        [DataMember(Name = "price")]
        public decimal Price { get; set; }

        //For deserialization only so warning 'never assigned'
        [DataMember(Name = "type")]
#pragma warning disable IDE0044 // Add readonly modifier
        private string TypeBuySellString; // { get; set; }
#pragma warning restore IDE0044 // Add readonly modifier

        [DataMember(Name = "amount")]
        public decimal Amount { get; set; }

        [DataMember(Name = "tid")]
        public long TID { get; set; }

        public DateTimeOffset Date { get => DateTimeOffset.FromUnixTimeSeconds(UnixDate); }
        public DateTimeOffset DateLocal { get => Date.ToLocalTime(); }
        public SellBuyEnum BuySell { get => TypeBuySellString[0] == 's' ? SellBuyEnum.Sell_Ask : SellBuyEnum.Buy_Bid; }

        [DataContract]
        public enum BuySellEnum
        {
            [EnumMember(Value = "buy")]
            Buy,
            [EnumMember(Value = "sell")]
            Sell
        }
    }
}
