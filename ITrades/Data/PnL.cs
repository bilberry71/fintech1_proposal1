﻿using System;

namespace Trades.Data
{
    /// <summary>
    /// Profit and Loss for a pair of realized orders
    /// </summary>
    public class PnL
    {
        public BBOrder BuyOrder { get; }
        public BBOrder SellOrder { get; }
        public decimal BuyPrice => BuyOrder?.Price ?? decimal.MaxValue;
        public decimal SellPrice => SellOrder?.Price ?? decimal.MinValue;
        public bool isProfitable => SellPrice > BuyPrice;
        public decimal Profit => (SellPrice - BuyPrice) * BuyOrder.Amount;
        public DateTimeOffset BuyTimeUTC => BuyOrder?.TimeUTC ?? DateTimeOffset.MaxValue;
        public DateTimeOffset SellTimeUTC => SellOrder?.TimeUTC ?? DateTimeOffset.MinValue;
        public TimeSpan Period => SellTimeUTC - BuyTimeUTC;
        public int OpenPositionsCount { get; }
        public string SID { get; }

        public PnL(BBOrder buyOrder, BBOrder sellOrder, int openPositionsCount, string SID = "")
        {
            BuyOrder = buyOrder ?? throw new ArgumentNullException(nameof(buyOrder));
            SellOrder = sellOrder ?? throw new ArgumentNullException(nameof(sellOrder));
            OpenPositionsCount = openPositionsCount;
            this.SID = SID;
        }


    }
}
