﻿using System;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace Trades.Data
{
    [DebuggerDisplay("{AskBid}|{Price}|TimeUTC")]
    public class BBOrder
    {
        public string FiatCcy { get; set; }

        public decimal Price { get; set; }

        public decimal Amount { get; set; }

        public SellBuyEnum AskBid { get; set; }

        public DateTimeOffset TimeUTC { get; set; }
        public DateTimeOffset TimeLocal => TimeUTC.ToLocalTime();
    }
}