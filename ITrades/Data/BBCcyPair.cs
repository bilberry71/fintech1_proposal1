﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trades.Data
{
    public class BBCcyPair
    {
        public string Pair { get { return CCy + Fiat; } }
        public string Fiat { get; private set; }
        public string CCy { get; private set; }
        public string CcyName { get; private set; }
        public DateTime Start { get; private set; }

        public static BBCcyPair BTCPLN = new BBCcyPair()
        {
            Fiat = "PLN",
            CCy = "BTC",
            CcyName = "BITCOIN",
            Start = new DateTime(2014, 03, 29)
        };
        public static BBCcyPair ETHPLN = new BBCcyPair()
        {
            Fiat = "PLN",
            CCy = "ETH",
            CcyName = "ETHEREUM",
            Start = new DateTime(2016, 4, 2)
        };
        public static BBCcyPair LSKPLN = new BBCcyPair()
        {
            Fiat = "PLN",
            CCy = "LSK",
            CcyName = "LISK",
            Start = new DateTime(2016, 6, 28)
        };
        public static BBCcyPair LTCPLN = new BBCcyPair()
        {
            Fiat = "PLN",
            CCy = "LTC",
            CcyName = "LITECOIN",
            Start = new DateTime(2014, 4, 4)
        };
        public static BBCcyPair GAMEPLN = new BBCcyPair()
        {
            Fiat = "PLN",
            CCy = "GAME",
            CcyName = "GAME",
            Start = new DateTime(2017, 9, 23)
        };
        public static BBCcyPair DASHPLN = new BBCcyPair()
        {
            Fiat = "PLN",
            CCy = "DASH",
            CcyName = "DASH",
            Start = new DateTime(2017, 9, 23)
        };
        public static BBCcyPair BCCPLN = new BBCcyPair()
        {
            Fiat = "PLN",
            CCy = "BCC",
            CcyName = "BITCOIN CASH",
            Start = new DateTime(2017, 8, 2)
        };
        public static BBCcyPair BTGPLN = new BBCcyPair()
        {
            Fiat = "PLN",
            CCy = "BTG",
            CcyName = "BITCOIN GOLD",
            Start = new DateTime(2017, 12, 1)
        };
        public static BBCcyPair KZCPLN = new BBCcyPair()
        {
            Fiat = "PLN",
            CCy = "KZC",
            CcyName = "KZCASH",
            Start = new DateTime(2018, 6, 11)
        };
        public static BBCcyPair XINPLN = new BBCcyPair()
        {
            Fiat = "PLN",
            CCy = "XIN",
            CcyName = "INFINITY ECONOMICS",
            Start = new DateTime(2018, 6, 11)
        };
        public static BBCcyPair XRPPLN = new BBCcyPair()
        {
            Fiat = "PLN",
            CCy = "XRP",
            CcyName = "RIPPLE",
            Start = new DateTime(2018, 3, 14)
        };
        public static BBCcyPair XMRPLN = new BBCcyPair()
        {
            Fiat = "PLN",
            CCy = "XMR",
            CcyName = "MONERO",
            Start = new DateTime(2018, 7, 3)
        };


        public static IEnumerable<BBCcyPair> GetBBCurrencies()
        {
            yield return BTCPLN;
            yield return ETHPLN;
            yield return LSKPLN;
            yield return LTCPLN;
            yield return GAMEPLN;
            yield return DASHPLN;
            yield return BCCPLN;
            yield return BTGPLN;
            yield return KZCPLN;
            yield return XINPLN;
            yield return XRPPLN;
            yield return XMRPLN;
        }
    }
}
