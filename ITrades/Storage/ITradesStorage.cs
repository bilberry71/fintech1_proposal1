﻿using System;
using System.Collections.Generic;
using Trades.Data;

namespace Trades.Storage
{
    public interface ITradesStorage
    {
        IEnumerable<BBTrade> GetTradesEnum(BBCcyPair ccyPair, DateTime since, DateTime to);
    }
}