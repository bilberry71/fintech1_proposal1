﻿using Trades.Data;
using Trades.Storage;
using Microsoft.Practices.Unity.Configuration;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using Unity;
using System.Collections.Generic;
using Trades.Strategy;

namespace AlgoBacktesting.CLI
{
    class Program
    {
        private static readonly string reportFileEnum = $@"..\Report {DateTime.Now:yyyyMMdd_HHmmss}.txt";
        private const decimal stopLossPercent = 10m; //10% drop then sell to stop loosing more
        private const decimal expectedIntrestRatePercent = 3m; //3% raise to sell
        private const decimal investAmount = 100m;
        private static DateTime _lastDate = DateTime.MinValue;
        private static StreamWriter report;

        static void Main(string[] args)
        {
            var container = new UnityContainer();
            var section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            section.Configure(container);
            using (report = File.CreateText(reportFileEnum))
            {
                ITradesStorage tfs = container.Resolve<ITradesStorage>();
                DateTime sinceDate = new DateTime(2017, 1, 1); 
                DateTime toDate = new DateTime(2017, 12, 31); 
                var trades = tfs.GetTradesEnum(BBCcyPair.BTCPLN, sinceDate, toDate);
                var @params = new List<DropStrategyParameter>() {
                        new DropStrategyParameter("5%60m", 5m, new TimeSpan(hours: 0, minutes: 60, seconds: 0)),
                        new DropStrategyParameter("10%600m", 10m, new TimeSpan(hours: 0, minutes: 600, seconds: 0)),
                    };
                var strategy = new WaitForMultiDropStrategy(@params, expectedIntrestRatePercent, stopLossPercent);
                var start = DateTime.Now;
                WriteLine($"STRATEGY running: Buy CRYPTO for {investAmount}PLN ");
                foreach (var par in @params)
                {
                    WriteLine($" - Strat {par.SID} = when drop > {par.DropPercent}% in {par.DropTime.ToString()} ");
                }
                WriteLine($"Sell WHEN raise > {expectedIntrestRatePercent}% OR drop > {stopLossPercent}%");
                WriteLine($"Since {sinceDate} to {toDate}");
                var lastTrade = strategy.ProcessTrades(trades, BuyAction, SellAction, ProgressStrategyEnum);
                WriteLine($"Took {DateTime.Now - start}                                                    ||");

                List<BBOrder> openLongPositions = strategy.OpenLongPositions.Select(t => t.Item1).ToList();
                PrintSummary(strategy.Profits, openLongPositions, lastTrade);
            }
        }
        private static BBOrder SellAction(BBTrade trade, BBOrder buyOrder)
        {
            var sellOrder = new BBOrder()
            {
                AskBid = SellBuyEnum.Sell_Ask,
                FiatCcy = "PLN",
                Price = trade.Price,
                Amount = buyOrder.Amount,
                TimeUTC = trade.Date
            };
            //WriteLine($"\nSELL: {sellOrder.Price:F3}{sellOrder.FiatCcy} / Am: {sellOrder.Amount:F3}");
            return sellOrder;
        }
        private static BBOrder BuyAction(BBTrade trade, BBTrade noord)
        {
            var buyOrder = new BBOrder()
            {
                AskBid = SellBuyEnum.Buy_Bid,
                FiatCcy = "PLN",
                Price = trade.Price,
                Amount = investAmount / trade.Price,
                TimeUTC = trade.Date
            };
            //WriteLine($"\nBUY: {buyOrder.Price:F3}{buyOrder.FiatCcy} / Am: {buyOrder.Amount:F3}");
            return buyOrder;
        }
        private static void ProgressStrategyEnum(long nr, BBTrade trade)
        {
            if (trade.DateLocal.Date > _lastDate)
            {
                _lastDate = trade.DateLocal.Date;
                Console.Write($"\r{nr:10} : {trade.TID:10} {trade.DateLocal}        \r");
            }
        }

        //REPORT
        const int PROFIT_COL_LEN = 11;
        const int TIME_COL_LEN = 26;
        const int NR_COL_LEN = 3;
        private static void PrintSummary(List<PnL> profits, List<BBOrder> openLongPositions, BBTrade lastTrade)
        {
            WriteLine($"----------PROFIT&LOSS-----------");
            int i = 0, maxPos = 0;
            decimal totalPnL = 0m;
            PrintRow("NR", "Profit", "SumProfit", "BUY Time", "SELL Time", "BUY price", "SELL pice", "#Open", "Strat");
            PrintSeparatorRow();
            foreach (var pnl in profits)
            {
                i++;
                totalPnL += pnl.Profit;
                maxPos = Math.Max(pnl.OpenPositionsCount, maxPos);
                decimal profit = pnl.Profit;
                DateTimeOffset buyTime = pnl.BuyTimeUTC.ToLocalTime();
                DateTimeOffset sellTime = pnl.SellTimeUTC.ToLocalTime();
                decimal buyPrice = pnl.BuyPrice;
                decimal sellPrice = pnl.SellPrice;
                PrintRow(i, totalPnL, profit, buyTime, sellTime, buyPrice, sellPrice, pnl.OpenPositionsCount, pnl.SID);

            }
            PrintSeparatorRow();
            WriteLine($"{"SUM",NR_COL_LEN}|{"SUB SUM".PadRight(PROFIT_COL_LEN, '-'),PROFIT_COL_LEN}|{totalPnL,PROFIT_COL_LEN:F5}|");
            PrintSeparatorRow();
            foreach (var ord in openLongPositions)
            {
                i++;
                decimal loss = (lastTrade.Price - ord.Price) * ord.Amount;
                totalPnL += loss;
                PrintRow(i, totalPnL, loss, ord.TimeLocal, DateTimeOffset.Now, ord.Price, lastTrade.Price, maxPos, string.Empty);
            }
            PrintSeparatorRow();
            WriteLine($"{"SUM",NR_COL_LEN}|{"TOTAL SUM".PadRight(PROFIT_COL_LEN, '-'),PROFIT_COL_LEN}|{totalPnL,PROFIT_COL_LEN:F5}|Invest:{maxPos,NR_COL_LEN}|");
            PrintSeparatorRow();
        }
        private static void PrintSeparatorRow()
        {
            PrintRow(
                "",
                "-".PadRight(PROFIT_COL_LEN, '-'),
                "-".PadRight(PROFIT_COL_LEN, '-'),
                "-".PadRight(TIME_COL_LEN, '-'),
                "-".PadRight(TIME_COL_LEN, '-'),
                "-".PadRight(PROFIT_COL_LEN, '-'),
                "-".PadRight(PROFIT_COL_LEN, '-'),
                "-".PadRight(NR_COL_LEN, '-'),
                "-".PadRight(NR_COL_LEN, '-')
                );
        }
        private static void PrintRow(string sNr, string sProfit, string sTotalProfit,
            string sBuyTime, string sSellTime, string sBuyPrice, string sSellPrice,
            string sOpenPosNr, string SID)
        {
            WriteLine(
                $"{sNr,NR_COL_LEN}|" +
                $"{sProfit,PROFIT_COL_LEN}|" +
                $"{sTotalProfit,PROFIT_COL_LEN}|" +
                $"{sBuyTime,TIME_COL_LEN}|" +
                $"{sSellTime,TIME_COL_LEN}|" +
                $"{sBuyPrice,PROFIT_COL_LEN}|" +
                $"{sSellPrice,PROFIT_COL_LEN}|" +
                $"{sOpenPosNr,NR_COL_LEN}|" +
                $"{SID,NR_COL_LEN}|"
                );
        }
        private static void PrintRow(int i, decimal totalPnL, decimal profit,
            DateTimeOffset buyTime, DateTimeOffset sellTime, decimal buyPrice, decimal sellPrice,
            int cntOpenPositions, string SID)
        {
            WriteLine(
                $"{i,NR_COL_LEN}|" +
                $"{profit,PROFIT_COL_LEN:F3}|" +
                $"{totalPnL,PROFIT_COL_LEN:F3}|" +
                $"{buyTime,TIME_COL_LEN}|" +
                $"{sellTime,TIME_COL_LEN}|" +
                $"{buyPrice,PROFIT_COL_LEN:F3}|" +
                $"{sellPrice,PROFIT_COL_LEN:F3}|" +
                $"{cntOpenPositions,NR_COL_LEN}|" +
                $"{SID,NR_COL_LEN}|"
                );
        }
        private static void WriteLine(string msg)
        {
            Console.WriteLine(msg);
            report?.WriteLine(msg);
        }
        private static void Write(string msg)
        {
            Console.Write(msg);
            report.Write(msg);
        }
    }
}
