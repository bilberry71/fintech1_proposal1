﻿using Trades.Data;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
namespace Trades.Storage
{
    public class FileFascade
    {
        private readonly string filePath;

        public FileFascade(string filePath)
        {
            this.filePath = filePath;
        }
        public List<BBTrade> GetTrades(string ccy, long tidSince = 0)
        {
            List<BBTrade> lst = new List<BBTrade>();
            using (var stream = File.OpenText(filePath).BaseStream)
            {
                DataContractJsonSerializer jsonSer = new DataContractJsonSerializer(typeof(List<BBTrade>));
                lst = (List<BBTrade>)jsonSer.ReadObject(stream);
            }
            if (tidSince > 0)
            {
                lst = lst.Where(t => t.TID > tidSince).ToList();
            }
            return lst;
        }
    }
}
