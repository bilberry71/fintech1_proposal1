﻿using Trades.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Trades.Storage
{
    public class TradesFileStorage : ITradesStorage
    {
        private readonly string storageFileFolder;

        public TradesFileStorage(string storageFileFolder)
        {
            this.storageFileFolder = storageFileFolder;
        }

        public IEnumerable<BBTrade> GetTradesEnum(BBCcyPair ccyPair, DateTime since, DateTime to)
        {
            List<BBTrade> retTrades = new List<BBTrade>();
            foreach (var f in GetFiles(ccyPair, since.Date, to.Date).OrderBy(f => f))
            {
                var fAPI = new FileFascade(Path.Combine(storageFileFolder, f));
                var list = fAPI.GetTrades(ccy: ccyPair.Pair);
                foreach (var t in list.Where(t => t.Date >= since && t.Date < to))
                {
                    yield return t;
                }
            }
        }

        private IEnumerable<string> GetFiles(BBCcyPair ccyPair, DateTime since, DateTime to)
        {
            foreach (var periodFilePath in Directory.EnumerateFiles(storageFileFolder, $"{ccyPair.Pair}_*.json", SearchOption.TopDirectoryOnly))
            {
                CultureInfo provider = CultureInfo.InvariantCulture;
                var fname = Path.GetFileName(periodFilePath);
                var match = Regex.Match(fname, $@"{ccyPair.Pair}+_(\d{{8}})_(\d{{8}})_\w+\.json");
                if (match.Groups.Count == 3)
                {
                    var fSince = DateTime.ParseExact(match.Groups[1].Value, "yyyyMMdd", provider);
                    var fTo = DateTime.ParseExact(match.Groups[2].Value, "yyyyMMdd", provider);
                    if (since >= fSince && since <= fTo || to >= fSince && to <= fTo || since <= fSince && to >= fTo)
                    {
                        yield return fname;
                    }
                }
            }
        }
    }
}
