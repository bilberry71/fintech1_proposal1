﻿using Trades.Data;

namespace Trades.Strategy
{
    public delegate BBOrder PutSellOrderDelegate(BBTrade triggeringTrade, BBOrder buyOrder);
    public delegate BBOrder PutBuyOrderDelegate(BBTrade triggeringTrade, BBTrade referenceTrade);
}
