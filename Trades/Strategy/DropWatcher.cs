﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Trades.Data;

namespace Trades.Strategy
{
    public partial class WaitForMultiDropStrategy
    {
        [DebuggerDisplay("{SID}|wait:{IsWaiting}|trades cnt:{tradesInPeriod}")]
        private class DropWatcher : IEquatable<DropWatcher>
        {
            static readonly DateTimeOffset dontWait = DateTimeOffset.MinValue;
            public decimal DropPercent { get => dropStrategyParameter.DropPercent; }
            public TimeSpan DropTime { get => dropStrategyParameter.DropTime; }
            public string SID { get => dropStrategyParameter.SID; }
            public bool IsWaiting { get => waitUntil != dontWait; }

            public readonly DropStrategyParameter dropStrategyParameter;
            private Queue<BBTrade> tradesInPeriod;
            private DateTimeOffset waitUntil;

            public DropWatcher(DropStrategyParameter dropStrategyParameter)
            {
                this.dropStrategyParameter = dropStrategyParameter;
            }
            public void Initialize()
            {
                tradesInPeriod = new Queue<BBTrade>();
                DontWait();
            }
            public void CutOrdersQueueToPeriod(DateTimeOffset now)
            {
                var firstInPeriod = now - DropTime;
                var first = tradesInPeriod.Peek();
                while (first.Date < firstInPeriod)
                {
                    tradesInPeriod.Dequeue();
                    first = tradesInPeriod.Peek();
                }
            }
            public void Enqueue(BBTrade tradeNow)
            {
                tradesInPeriod.Enqueue(tradeNow);
            }

            public void DontWait()
            {
                waitUntil = dontWait;
            }

            public bool ShouldStillWaitingFor(DateTimeOffset date)
            {
                return waitUntil > date;
            }

            public void WaitUntil(DateTimeOffset dateTimeOffset)
            {
                waitUntil = dateTimeOffset;
            }

            public override bool Equals(object obj)
            {
                var watcher = obj as DropWatcher;
                return watcher != null &&
                       SID == watcher.SID;
            }

            public static bool operator ==(DropWatcher watcher1, DropWatcher watcher2)
            {
                return EqualityComparer<DropWatcher>.Default.Equals(watcher1, watcher2);
            }

            public static bool operator !=(DropWatcher watcher1, DropWatcher watcher2)
            {
                return !(watcher1 == watcher2);
            }

            public BBTrade GreaterPriceTradeInPeriod(decimal expectedMaxPrice)
            {
                return tradesInPeriod.FirstOrDefault(t => t.Price >= expectedMaxPrice);
            }

            public bool Equals(DropWatcher other)
            {
                return other != null &&
                       DropPercent == other.DropPercent &&
                       DropTime.Equals(other.DropTime) &&
                       SID == other.SID &&
                       IsWaiting == other.IsWaiting &&
                       EqualityComparer<DropStrategyParameter>.Default.Equals(dropStrategyParameter, other.dropStrategyParameter) &&
                       EqualityComparer<Queue<BBTrade>>.Default.Equals(tradesInPeriod, other.tradesInPeriod) &&
                       waitUntil.Equals(other.waitUntil);
            }

            public override int GetHashCode()
            {
                var hashCode = 1738351397;
                hashCode = hashCode * -1521134295 + DropPercent.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<TimeSpan>.Default.GetHashCode(DropTime);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(SID);
                hashCode = hashCode * -1521134295 + IsWaiting.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<DropStrategyParameter>.Default.GetHashCode(dropStrategyParameter);
                hashCode = hashCode * -1521134295 + EqualityComparer<Queue<BBTrade>>.Default.GetHashCode(tradesInPeriod);
                hashCode = hashCode * -1521134295 + EqualityComparer<DateTimeOffset>.Default.GetHashCode(waitUntil);
                return hashCode;
            }
        }


    }
}
