﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trades.Data;

namespace Trades.Strategy
{
    public partial class WaitForMultiDropStrategy
    {
        public List<Tuple<BBOrder, DropStrategyParameter>> OpenLongPositions => new List<Tuple<BBOrder, DropStrategyParameter>>(openBuyOrders);
        public List<PnL> Profits => new List<PnL>(pnls);
        public List<DropStrategyParameter> DropParameters { get; }

        private readonly decimal intrestRatePercent;
        private readonly decimal stopLossPercent;
        private readonly decimal interestRatePlus100Percent;
        private readonly decimal stopLossPlus100Percent;
        private readonly List<DropWatcher> dropWatchers;
        private List<Tuple<BBOrder, DropStrategyParameter>> openBuyOrders;
        private List<PnL> pnls;

        /// <summary>
        /// </summary>
        /// <param name="dropPercent">40m = 40% drop of price to generate buy signal</param>
        /// <param name="dropTime">generate buy signal if dropPercent is achieved during dropTime period</param>
        /// <param name="sID">string ID of Drop param</param>
        /// <param name="intrestRatePercent">4m = 4% intrest rate margin to generate sell signal</param>
        /// <param name="stopLossPercent">when to sell with loss</param>
        public WaitForMultiDropStrategy(List<DropStrategyParameter> dropParameters, decimal intrestRatePercent, decimal stopLossPercent = 0m)
        {
            DropParameters = new List<DropStrategyParameter>(dropParameters ?? throw new ArgumentNullException(nameof(dropParameters)));
            dropWatchers = DropParameters.Select(p => new DropWatcher(p)).ToList();
            this.intrestRatePercent = intrestRatePercent;
            this.stopLossPercent = stopLossPercent;
            interestRatePlus100Percent = 1m + intrestRatePercent / 100m;
            stopLossPlus100Percent = 1m - stopLossPercent / 100;
        }

        public BBTrade ProcessTrades(IEnumerable<BBTrade> trades, PutBuyOrderDelegate buyAction, PutSellOrderDelegate sellAction, Action<long, BBTrade> progressAction = null)
        {
            if (trades == null)
            {
                throw new ArgumentNullException(nameof(trades));
            }
            if (buyAction == null)
            {
                throw new ArgumentNullException(nameof(buyAction));
            }
            if (sellAction == null)
            {
                throw new ArgumentNullException(nameof(sellAction));
            }

            dropWatchers.ForEach(p => p.Initialize());
            openBuyOrders = new List<Tuple<BBOrder, DropStrategyParameter>>();
            pnls = new List<PnL>();
            long tradeNr = 0;
            BBTrade lastTrade = null;
            foreach (var tradeNow in trades)
            {
                lastTrade = tradeNow;
                foreach (DropWatcher dropWatcher in dropWatchers)
                {
                    dropWatcher.Enqueue(tradeNow);
                    dropWatcher.CutOrdersQueueToPeriod(now: tradeNow.Date);
                    if (!dropWatcher.ShouldStillWaitingFor(tradeNow.Date))
                    {
                        dropWatcher.DontWait();
                    }
                    if (!dropWatcher.IsWaiting && GenerateBuyEvent(buyAction, tradeNow, dropWatcher))
                    {
                        dropWatcher.WaitUntil(tradeNow.Date + dropWatcher.DropTime);
                        //reset drop period for other watchers
                        dropWatchers.Where(dw => dw != dropWatcher).ToList().ForEach(dw => dw.Initialize());
                        break;
                    }
                }
                if (IsAnyBuyOrderOpen())
                {
                    GenerateSellEvent(sellAction, tradeNow: tradeNow);
                }
                progressAction?.Invoke(tradeNr++, tradeNow);
            }
            return lastTrade;
        }


        private bool IsAnyBuyOrderOpen()
        {
            return openBuyOrders.Any(o => o.Item1.AskBid == SellBuyEnum.Buy_Bid);
        }

        private void GenerateSellEvent(PutSellOrderDelegate sellAction, BBTrade tradeNow)
        {
            var ordersToDel = new List<Tuple<BBOrder, DropStrategyParameter>>();
            foreach (var tupleOrder in openBuyOrders.Where(o => o.Item1.AskBid == SellBuyEnum.Buy_Bid))
            {
                var buyOrder = tupleOrder.Item1;
                if (tradeNow.Price >= buyOrder.Price * interestRatePlus100Percent      // make profit
                    || stopLossPercent > 0 && tradeNow.Price <= buyOrder.Price * stopLossPlus100Percent //stop loosing
                    )
                {
                    var sellOrder = sellAction(tradeNow, buyOrder);
                    if (sellOrder.AskBid != SellBuyEnum.Sell_Ask)
                    {
                        throw new ApplicationException("You must SELL not buy");
                    }
                    var pnl = new PnL(buyOrder, sellOrder, openPositionsCount: openBuyOrders.Count, SID: tupleOrder.Item2.SID);
                    pnls.Add(pnl);
                    ordersToDel.Add(tupleOrder);
                }
            }
            foreach (var ord in ordersToDel)
            {
                openBuyOrders.Remove(ord);
            }
        }

        private bool GenerateBuyEvent(PutBuyOrderDelegate buyAction, BBTrade now, DropWatcher dropWatcher)
        {
            bool generated = false;
            decimal expectedMaxPrice = now.Price * (1m + dropWatcher.DropPercent / 100m);
            var maxPriceTrade = dropWatcher.GreaterPriceTradeInPeriod(expectedMaxPrice);
            if (maxPriceTrade != null)
            {
                BBOrder order = buyAction(triggeringTrade: now, referenceTrade: maxPriceTrade);
                if (order.AskBid != SellBuyEnum.Buy_Bid)
                {
                    throw new ApplicationException("You must BUY not sell");
                }
                openBuyOrders.Add(new Tuple<BBOrder, DropStrategyParameter>(order, dropWatcher.dropStrategyParameter));
                generated = true;
            }
            return generated;
        }


    }
}
