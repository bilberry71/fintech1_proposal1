﻿using System;

namespace Trades.Strategy
{
    public class DropStrategyParameter
    {
        public DropStrategyParameter(string sID, decimal dropPercent, TimeSpan dropTime)
        {
            DropPercent = dropPercent;
            DropTime = dropTime;
            SID = sID;
        }

        public decimal DropPercent { get; }
        public TimeSpan DropTime { get; }
        public string SID { get; }
    }
}
