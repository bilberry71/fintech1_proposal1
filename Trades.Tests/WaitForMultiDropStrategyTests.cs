﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using Trades.Data;
using Trades.Strategy;

namespace Trades.Tests
{
    [TestFixture]
    public class WaitForMultiDropStrategyTests
    {
        private const decimal IntrestRatePercent = 5m;
        private int numOfSells;
        private int numOfBuys;
        private int progressCnt;

        [SetUp]
        public void Setup()
        {
            numOfSells = 0;
            numOfBuys = 0;
            progressCnt = 0;
        }

        [TestCaseSource("TradesData")]
        public Tuple<int, int, int> RunningStrategy_BuySellEventCalled(List<BBTrade> trades)
        {
            //Arrange
            var strategy = GetStrategy();

            //Act
            strategy.ProcessTrades(trades, BuyAction, SellAction, ProgressAction);

            //Assert
            return Tuple.Create(numOfBuys, numOfSells, progressCnt);
        }

        private WaitForMultiDropStrategy GetStrategy()
        {
            var @params = new List<DropStrategyParameter>() {
                new DropStrategyParameter("10%1h", 10m, new TimeSpan(hours: 1, minutes: 0, seconds: 0))
            };
            var strategy = new WaitForMultiDropStrategy(@params, IntrestRatePercent, stopLossPercent: 20m);
            return strategy;
        }

        /// <summary>
        /// Values returned by test cases are: count of buy events, 
        /// count of sell events, count of progress events
        /// </summary>
        /// <returns></returns>
        public static List<TestCaseData> TradesData()
        {
            var testCases = new List<TestCaseData>();
            testCases.Add(new TestCaseData(
                GetTradesCase1_0buy0sell())
            .Returns(Tuple.Create(0, 0, 4))
            .SetName("Case1: No buy/sell triggered")
            );
            testCases.Add(new TestCaseData(
                GetTradesCase2_1buy0sell())
            .Returns(Tuple.Create(1, 0, 4))
            .SetName("Case 2: 1 buy event no sell event triggered")
            );
            testCases.Add(new TestCaseData(
                GetTradesCase3_1buy1sell())
            .Returns(Tuple.Create(1, 1, 4))
            .SetName("Case 3: 1 buy event, 1 sell event triggered")
            );
            testCases.Add(new TestCaseData(
                GetTradesCase4_1buy1sellStopLoss())
            .Returns(Tuple.Create(1, 1, 4))
            .SetName("Case 4: 1 buy event, 1 sell Stop-loss event triggered")
            );
            return testCases;
        }
        private BBOrder SellAction(BBTrade triggeringTrade, BBOrder buyOrder)
        {
            numOfSells++;
            return new BBOrder() { AskBid = SellBuyEnum.Sell_Ask, Price = triggeringTrade.Price, Amount = buyOrder.Amount, TimeUTC = triggeringTrade.Date };
        }
        private BBOrder BuyAction(BBTrade triggeringTrade, BBTrade referenceTrade)
        {
            numOfBuys++;
            return new BBOrder() { AskBid = SellBuyEnum.Buy_Bid, Price = triggeringTrade.Price, Amount = 1.0m, TimeUTC = triggeringTrade.Date };
        }
        private BBOrder WrongSellAction(BBTrade trade, BBOrder buyOrder)
        {
            numOfSells++;
            return new BBOrder() { AskBid = SellBuyEnum.Buy_Bid, Price = trade.Price, Amount = buyOrder.Amount };
        }
        private BBOrder WrongBuyAction(BBTrade triggeringTrade, BBTrade referenceTrade)
        {
            numOfBuys++;
            return new BBOrder() { AskBid = SellBuyEnum.Sell_Ask, Price = triggeringTrade.Price, Amount = 1.0m };
        }
        private void ProgressAction(long arg1, BBTrade arg2)
        {
            progressCnt++;
        }
        private static List<BBTrade> GetTradesCase1_0buy0sell()
        {
            return new List<BBTrade>() {
                    new BBTrade { TID = 1, Price = 2.0m, Amount = 1m, UnixDate = GetBaseTimeWithDelta(TimeSpan.Zero) },
                    new BBTrade { TID = 2, Price = 10.0m, Amount = 1m, UnixDate = GetBaseTimeWithDelta(new TimeSpan(hours:1, minutes:0, seconds:0)) },
                    new BBTrade { TID = 3, Price = 9.5m, Amount = 1m, UnixDate = GetBaseTimeWithDelta(new TimeSpan(hours:2, minutes:0, seconds:0)) },
                    new BBTrade { TID = 4, Price = 8.9m, Amount = 1m, UnixDate = GetBaseTimeWithDelta(new TimeSpan(hours:3, minutes:0, seconds:0)) }
                };
        }
        private static List<BBTrade> GetTradesCase2_1buy0sell()
        {
            return new List<BBTrade>() {
                    new BBTrade { TID = 1, Price = 2.0m, Amount = 1m, UnixDate = GetBaseTimeWithDelta(TimeSpan.Zero) },
                    new BBTrade { TID = 2, Price = 10.0m, Amount = 1m, UnixDate = GetBaseTimeWithDelta(new TimeSpan(hours:1, minutes:0, seconds:0)) },
                    new BBTrade { TID = 3, Price = 9.0m, Amount = 1m, UnixDate = GetBaseTimeWithDelta(new TimeSpan(hours:2, minutes:0, seconds:0)) },
                    new BBTrade { TID = 4, Price = 9.7m, Amount = 1m, UnixDate = GetBaseTimeWithDelta(new TimeSpan(hours:3, minutes:0, seconds:0)) }
                };
        }
        private static List<BBTrade> GetTradesCase3_1buy1sell()
        {
            return new List<BBTrade>() {
                    new BBTrade { TID = 1, Price = 2.0m, Amount = 1m, UnixDate = GetBaseTimeWithDelta(TimeSpan.Zero) },
                    new BBTrade { TID = 2, Price = 10.0m, Amount = 1m, UnixDate = GetBaseTimeWithDelta(new TimeSpan(hours:1, minutes:0, seconds:0)) },
                    new BBTrade { TID = 3, Price = 9.0m, Amount = 1m, UnixDate = GetBaseTimeWithDelta(new TimeSpan(hours:2, minutes:0, seconds:0)) }, 
                    new BBTrade { TID = 4, Price = 9.45m, Amount = 1m, UnixDate = GetBaseTimeWithDelta(new TimeSpan(hours:3, minutes:0, seconds:0)) } 
                };
        }
        private static List<BBTrade> GetTradesCase4_1buy1sellStopLoss()
        {
            return new List<BBTrade>() {
                    new BBTrade { TID = 1, Price = 2.0m, Amount = 1m, UnixDate = GetBaseTimeWithDelta(TimeSpan.Zero) },
                    new BBTrade { TID = 2, Price = 12.0m, Amount = 1m, UnixDate = GetBaseTimeWithDelta(new TimeSpan(hours:1, minutes:0, seconds:0)) },
                    new BBTrade { TID = 3, Price = 10.0m, Amount = 1m, UnixDate = GetBaseTimeWithDelta(new TimeSpan(hours:2, minutes:0, seconds:0)) },
                    new BBTrade { TID = 4, Price = 8.0m, Amount = 1m, UnixDate = GetBaseTimeWithDelta(new TimeSpan(hours:2, minutes:1, seconds:0)) }
                };
        }
        private static long GetBaseTimeWithDelta(TimeSpan timespan)
        {
            return (new DateTimeOffset(2015, 1, 1, 0, 0, 0, 0, TimeSpan.Zero) + timespan).ToUnixTimeSeconds();
        }
    }
}
