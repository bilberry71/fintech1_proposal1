# Fintech Basics I Workshop

## Build and run solution

1. Select **Downloads** and click the link 'Download repository' (or Clone in Git)
2. Get **.NET Core 3.0** and recompile
3. Get data from [Fintech_Data1](https://bitbucket.org/bilberry71/fintech_data1/downloads)
and extract to a folder
4. Set **storageFileFolder** parameter in App.config to point to extracted files
5. Run console application to see results (P&L)
